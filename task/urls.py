from django.conf.urls import patterns, include, url
from . import settings
import django_cron

from django.contrib import admin
admin.autodiscover()
#django_cron.autodiscover()
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'task.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('social_auth.urls')),
    url(r'^$', 'login.views.home', name='home'),
    url(r'^logout$', 'login.views.logout_view', name='logout'),
)

